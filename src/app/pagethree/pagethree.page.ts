import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-pagethree',
  templateUrl: './pagethree.page.html',
  styleUrls: ['./pagethree.page.scss'],
})
export class PagethreePage implements OnInit {
  data: any;

  constructor(private newsService: NewsService) {}

  ngOnInit() {
    this.newsService
    .getData('top-headlines?sources=techcrunch')
    .subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
}
