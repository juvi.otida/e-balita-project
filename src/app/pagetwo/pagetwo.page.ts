import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
@Component({
  selector: 'app-pagetwo',
  templateUrl: './pagetwo.page.html',
  styleUrls: ['./pagetwo.page.scss'],
})
export class PagetwoPage implements OnInit {
  data: any;

  constructor(private newsService: NewsService) {}

  ngOnInit() {
    this.newsService
    .getData('everything?domains=wsj.com')
    .subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
}
