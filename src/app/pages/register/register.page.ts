import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(private router: Router) { }
  gotologin(){
    this.router.navigate(['/login']);
  }
  public form = {
    name:"",
    email_address: "",
    password: "",
    confirm_password: "",
    phone_number: "",
    street: "",
    city: "",
    state: "",
    zip_code: ""

  }

  ngOnInit() {
  }
    print(){
    console.log(this.form);
}
}
