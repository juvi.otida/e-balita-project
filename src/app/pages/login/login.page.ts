import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router: Router) { }
  gotoregister(){
    this.router.navigate(['/register']);
  }
  gotomain(){
    this.router.navigate(['/main']);
  }

  public form = {
    email:"",
    password: ""

  }

  ngOnInit() {
  }
    print(){
    console.log(this.form);
}
}
