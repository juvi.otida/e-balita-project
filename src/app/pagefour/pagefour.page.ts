import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-pagefour',
  templateUrl: './pagefour.page.html',
  styleUrls: ['./pagefour.page.scss'],
})
export class PagefourPage implements OnInit {
  data: any;

  constructor(private newsService: NewsService) {}
  
  ngOnInit() {
    this.newsService
    .getData('everything?q=apple&from=2021-07-17&to=2021-07-17&sortBy=popularity')
    .subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
}

