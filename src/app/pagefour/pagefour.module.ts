import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagefourPageRoutingModule } from './pagefour-routing.module';

import { PagefourPage } from './pagefour.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagefourPageRoutingModule
  ],
  declarations: [PagefourPage]
})
export class PagefourPageModule {}
