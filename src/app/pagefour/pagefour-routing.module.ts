import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagefourPage } from './pagefour.page';

const routes: Routes = [
  {
    path: '',
    component: PagefourPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagefourPageRoutingModule {}
