import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import {Router } from '@angular/router';

@Component({
  selector: 'app-pageone',
  templateUrl: './pageone.page.html',
  styleUrls: ['./pageone.page.scss'],
})
export class PageonePage implements OnInit {
  data: any;

  constructor(private newsService: NewsService, private router: Router) {}
  
  ngOnInit() {
    this.newsService
    .getData('top-headlines?country=us&category=business')
    .subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
  onGoToNewsSinglePage(article){
    this.newsService.currentArticle = article;
    this.router.navigate(['/news-single']);
  }
}
