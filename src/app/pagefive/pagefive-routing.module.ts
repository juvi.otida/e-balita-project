import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagefivePage } from './pagefive.page';

const routes: Routes = [
  {
    path: '',
    component: PagefivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagefivePageRoutingModule {}
