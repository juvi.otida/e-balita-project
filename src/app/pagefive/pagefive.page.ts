import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-pagefive',
  templateUrl: './pagefive.page.html',
  styleUrls: ['./pagefive.page.scss'],
})
export class PagefivePage implements OnInit {
  data: any;

  constructor(private newsService: NewsService) {}
  
  ngOnInit() {
    this.newsService
    .getData('everything?q=tesla&from=2021-06-25&sortBy=publishedAt')
    .subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
}
