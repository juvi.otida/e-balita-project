import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagefivePageRoutingModule } from './pagefive-routing.module';

import { PagefivePage } from './pagefive.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagefivePageRoutingModule
  ],
  declarations: [PagefivePage]
})
export class PagefivePageModule {}
