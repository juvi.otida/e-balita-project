import { Component } from '@angular/core';
import {Platform} from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  pages:any[]=[];
  constructor(
    private platform: Platform,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.pages = [{
        pagename:"Top News",
        icon:"newspaper",
        url:"/pageone"
      }, {
        pagename:"Journals",
        icon:"folder-open",
        url:"/pagetwo"
      }, {
        pagename:"Business",
        icon:"briefcase",
        url:"/pagethree"
      }, {
        pagename:"Technology",
        icon:"phone-portrait",
        url:"/pagefour"
      }, {
        pagename:"Auto",
        icon:"car-sport",
        url:"/pagefive"
    }]
    });
  }
  Goto(page)
  {
    this.router.navigate([page.url]);
  }
}
